package com.victor.imc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calcularIMC(View view) {

        EditText Peso = findViewById(R.id.EditTextPeso);
        EditText Altura = findViewById(R.id.EditTextAltura);
        TextView mostraIMC = findViewById(R.id.textViewResultadoIMC);
        ImageView Imagem = findViewById(R.id.imageView);
        double imc;

        Double peso = Double.parseDouble(Peso.getText().toString());
        Double altura = Double.parseDouble(Altura.getText().toString());
        imc = peso / (altura * altura);

        String resultado;
        if (imc < 18.5) {
            resultado = "IMC: "+imc+", Abaixo do peso";
            Imagem.setImageResource(R.drawable.abaixopeso);
        }else if (18.6 <= imc && imc <= 24.9){
            resultado = "IMC: "+imc+", Peso ideal (parabéns)";
            Imagem.setImageResource(R.drawable.normal);
        }else if (25.0 <= imc && imc <= 29.9){
            resultado = "IMC: "+imc+", Levemente acima do peso";
            Imagem.setImageResource(R.drawable.sobrepeso);
        }else if (30.0 <= imc && imc <= 34.9){
            resultado = "IMC: "+imc+", Obesidade grau I";
            Imagem.setImageResource(R.drawable.obesidade1);
        }else if (35.0 <= imc && imc <= 39.9){
            resultado = "IMC: "+imc+", Obesidade grau II (severa)";
            Imagem.setImageResource(R.drawable.obesidade2);
        }else{
            resultado = "IMC: "+imc+", Obesidade III (mórbida)";
            Imagem.setImageResource(R.drawable.obesidade3);
        }
        mostraIMC.setText(resultado);
    }
}